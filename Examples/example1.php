<?php

require_once(dirname(__FILE__) . '/../BookFinder/autoload.php');

$config = new \BookFinder\Entities\Config();
$config->addKeyValue('FiktivneKnihySk_URL', 'https://bookfinder.itea.sk/Stores/storeA.json')
    ->addKeyValue('NonRealBookShop_URL', 'https://bookfinder.itea.sk/Stores/storeB.json');

$bookFinder = new \BookFinder\BookFinder($config);
$listOfBooks = $bookFinder->searchBook('Harry Potter', \BookFinder\Interfaces\Enums\CurrencyEnum::EUR);
foreach ($listOfBooks as $book) {
    echo $book->getName() . ' - ' . $book->getPrice()->getAmount() . ' ' . $book->getPrice()->getCurrency()->value . "\n";
}