<?php

declare(strict_types=1);

namespace BookFinder\Exceptions;

class ImplementationDoesNotExist extends \Exception
{

    private string $implementationName;

    public function __construct(string $implementationName)
    {
        $this->implementationName = $implementationName;
        $message = 'Implemntation ' . $implementationName . ' does not exists';
        $code = 1;
        parent::__construct(
            $message,
            $code
        );
    }

    /**
     * @return string
     */
    public function getImplementationName(): string
    {
        return $this->implementationName;
    }

}