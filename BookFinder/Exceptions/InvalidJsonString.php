<?php

declare(strict_types=1);

namespace BookFinder\Exceptions;

use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;

class InvalidJsonString extends \Exception
{

    public function __construct()
    {
        $message = 'Invalid Json string';
        $code = 2;
        parent::__construct(
            $message,
            $code
        );
    }
}