<?php

declare(strict_types=1);

namespace BookFinder\Exceptions;

use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;

class ParsePriceError extends \Exception
{
    private mixed $price;

    public function __construct($price)
    {
        $this->price = $price;
        $code = 5;
        $message = 'Problem parse price: ' . $price;
        parent::__construct(
            $message,
            $code
        );
    }

    /**
     * @return mixed
     */
    public function getPrice(): mixed
    {
        return $this->price;
    }
}