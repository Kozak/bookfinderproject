<?php

declare(strict_types=1);

namespace BookFinder\Exceptions;

class MissingConfigKey extends \Exception
{
    private ?string $key;

    public function __construct($key)
    {
        $this->key = $key;
        $message = 'Missing config key: ' . $key;
        $code = 6;
        parent::__construct(
            $message,
            $code
        );
    }

    /**
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->key;
    }
}