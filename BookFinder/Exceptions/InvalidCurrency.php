<?php

declare(strict_types=1);

namespace BookFinder\Exceptions;

use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;

class InvalidCurrency extends \Exception
{
    private ?string $currency;

    public function __construct($currency)
    {
        $this->currency = $currency;
        $message = 'Invalid currency: ' . $currency;
        $code = 3;
        parent::__construct(
            $message,
            $code
        );
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }
}