<?php

declare(strict_types=1);

namespace BookFinder\Exceptions;

class InvalidResponse extends \Exception
{

    public function __construct($message)
    {
        $code = 3;
        parent::__construct(
            $message,
            $code
        );
    }
}