<?php

declare(strict_types=1);

namespace BookFinder;

use BookFinder\BookstoreClients\BookstoreClientFactory;
use BookFinder\Entities\ListOfBooks;
use BookFinder\Entities\Config;
use BookFinder\Interfaces\Entities\ListOfBookstores;
use BookFinder\Interfaces\Enums\CurrencyEnum;

class BookFinder implements \BookFinder\Interfaces\BookFinder
{
    private Config $config;

    public function __construct(\BookFinder\Interfaces\Entities\Config $config)
    {
        $this->config = $config;
    }

    public function searchBook(
        string $name,
        CurrencyEnum $sortCurrency = CurrencyEnum::EUR,
        ?ListOfBookstores $onlyBookStoresImplems = null
    ): ListOfBooks {
        $bookstoreClientFactory = new BookstoreClientFactory($this->config);
        $listOfBooks = new ListOfBooks();
        if (!is_null($onlyBookStoresImplems)) {
            $listOfBooksStores = $onlyBookStoresImplems;
        } else {
            $listOfBooksStores = $bookstoreClientFactory->listOfBookstores();
        }
        foreach ($listOfBooksStores as $bookstore) {
            ;
            $bookStoreClient = $bookstoreClientFactory->getBookstoreClient($bookstore->getImplemName());
            $currentBookStoreBookList = $bookStoreClient->listOfBooks($name);
            $listOfBooks->addBooks($currentBookStoreBookList);
        }
        $listOfBooks->sort($sortCurrency);
        return $listOfBooks;
    }
}
