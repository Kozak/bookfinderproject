<?php

spl_autoload_register('bookfinder_autoloader');

/**
 * @param string $class The fully-qualified class name.
 * @return void
 */
function bookfinder_autoloader(string $class): void
{
    $class_path = str_replace('\\', '/', $class);
    $class_path = preg_replace('/^BookFinder/', '', $class_path);

    $file = __DIR__ . '/' . $class_path . '.php';
    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
}
