<?php

declare(strict_types=1);

namespace BookFinder\BookstoreClients;

use BookFinder\Entities\ListOfBookstores;
use BookFinder\Entities\Config;
use BookFinder\Exceptions\ImplementationDoesNotExist;
use BookFinder\Interfaces\BookstoreClient;

class BookstoreClientFactory
{
    private array $bookStoreInstances = [];
    private Config $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function getBookstoreClient(string $bookstoreImplemName): BookstoreClient
    {
        if (isset($this->bookStoreInstances[$bookstoreImplemName])) {
            return $this->bookStoreInstances[$bookstoreImplemName];
        }

        if (class_exists('BookFinder::BookstoreClients::' . $bookstoreImplemName)) {
            throw new ImplementationDoesNotExist($bookstoreImplemName);
        }

        $bookStoreClientClass = '\BookFinder\BookstoreClients\\' . $bookstoreImplemName;
        $this->bookStoreInstances[$bookstoreImplemName] = new $bookStoreClientClass($this->config);
        return $this->bookStoreInstances[$bookstoreImplemName];
    }

    /**
     * Automatic loading of supported implementations for bookstores
     * @return ListOfBookstores
     */
    public function listOfBookstores(): ListOfBookstores
    {
        $listOfBookstores = new ListOfBookstores();
        $directoryOfClients = dirname(__FILE__);
        $files = scandir($directoryOfClients);
        foreach ($files as $file) {
            if (in_array($file, ['.', '..', 'BookstoreClientFactory.php'])) {
                continue;
            }
            $className = preg_replace('/\.php$/', '', $file);
            $bookStoreClientClass = '\BookFinder\BookstoreClients\\' . $className;
            if (class_exists($bookStoreClientClass)) {
                $bookStore = $bookStoreClientClass::getBookStore();
                $listOfBookstores->addBookstore($bookStore);
            }
        }
        return $listOfBookstores;
    }
}
