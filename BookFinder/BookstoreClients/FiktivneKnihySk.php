<?php

declare(strict_types=1);

namespace BookFinder\BookstoreClients;

use BookFinder\Entities\Book;
use BookFinder\Entities\Bookstore;
use BookFinder\Entities\ListOfBooks;
use BookFinder\Interfaces\Entities\Config;
use BookFinder\Exceptions\InvalidResponse;
use BookFinder\Helpers\CurlRequest;
use BookFinder\Helpers\OtherFunctions;
use BookFinder\Interfaces\BookstoreClient;

class FiktivneKnihySk implements BookstoreClient
{
    private string $storeUrl;

    public function __construct(Config $config)
    {
        $this->storeUrl = $config->getValue('FiktivneKnihySk_URL');
    }

    public static function getBookStore(): Bookstore
    {
        $explodeParts = explode('\\', self::class);
        $implemName = array_pop($explodeParts);
        return new Bookstore('Fiktívne kníhkupectvo 1', $implemName);
    }

    public function listOfBooks(string $searchName): ListOfBooks
    {
        $curlRequest = new CurlRequest($this->storeUrl);
        $response = $curlRequest->request(['nazov' => $searchName]);
        $jsonData = OtherFunctions::jsonToArray($response);

        if (!isset($jsonData['data'])) {
            throw new InvalidResponse('Missing attribute: data');
        }

        if (!is_array($jsonData['data'])) {
            throw new InvalidResponse('Wrong attribute: data');
        }

        $listOfBooks = new ListOfBooks();
        $bookStore = self::getBookStore();

        foreach ($jsonData['data'] as $jsonBook) {
            if (!isset($jsonBook['nazov']) || !isset($jsonBook['cena'])) {
                throw new InvalidResponse('Missing attributes for book');
            }
            /* The price in the response is not defined as a value and a currency. Therefore, we have to split it with a helper function */
            $bookPrice = OtherFunctions::parsePrice($jsonBook['cena']);
            $book = new Book($jsonBook['nazov'], $bookPrice, $bookStore);
            $listOfBooks->addBook($book);
        }

        return $listOfBooks;
    }
}
