<?php

declare(strict_types=1);

namespace BookFinder\BookstoreClients;

use BookFinder\Entities\Book;
use BookFinder\Entities\Bookstore;
use BookFinder\Entities\ListOfBooks;
use BookFinder\Entities\Price;
use BookFinder\Exceptions\InvalidResponse;
use BookFinder\Helpers\CurlRequest;
use BookFinder\Helpers\OtherFunctions;
use BookFinder\Interfaces\BookstoreClient;
use BookFinder\Interfaces\Entities\Config;

class NonRealBookShop implements BookstoreClient
{
    private string $storeUrl;

    public function __construct(Config $config)
    {
        $this->storeUrl = $config->getValue('NonRealBookShop_URL');
    }

    public static function getBookStore(): Bookstore
    {
        $explodeParts = explode('\\', self::class);
        $implemName = array_pop($explodeParts);
        return new Bookstore('Fiktívne kníhkupectvo 2', $implemName);
    }

    public function listOfBooks(string $searchName): ListOfBooks
    {
        $curlRequest = new CurlRequest($this->storeUrl);
        $response = $curlRequest->request(['title' => $searchName]);
        $jsonData = OtherFunctions::jsonToArray($response);

        $listOfBooks = new ListOfBooks();
        $bookStore = self::getBookStore();

        foreach ($jsonData as $jsonBook) {
            if (!isset($jsonBook['title']) || !isset($jsonBook['price']) || !isset($jsonBook['currency'])) {
                throw new InvalidResponse('Missing attributes for book');
            }
            if (!is_numeric($jsonBook['price'])) {
                throw new InvalidResponse('Price of book is not numeric');
            }
            $bookPrice = new Price((float)$jsonBook['price'], $jsonBook['currency']);
            $book = new Book($jsonBook['title'], $bookPrice, $bookStore);
            $listOfBooks->addBook($book);
        }

        return $listOfBooks;
    }
}
