<?php

declare(strict_types=1);

namespace BookFinder\Helpers;

use BookFinder\Entities\Price;
use BookFinder\Exceptions\InvalidJsonString;
use BookFinder\Exceptions\ParsePriceError;

class OtherFunctions
{
    public static function jsonToArray($string): array
    {
        $jsonData = json_decode($string, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidJsonString();
        }
        return $jsonData;
    }

    public static function parsePrice(string $fullPrice): Price
    {
        $matches = [];
        if (!preg_match('/([0-9\.]+)([^0-9\.]+)/', $fullPrice, $matches) || count($matches) != 3) {
            throw new ParsePriceError($fullPrice);
        }
        return new Price((float)$matches[1], $matches[2]);
    }
}
