<?php

declare(strict_types=1);

namespace BookFinder\Helpers;

class CurlRequest
{
    private string $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function request(array $params): string
    {
        $paramsString = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '?' . $paramsString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}