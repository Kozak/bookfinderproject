<?php

declare(strict_types=1);

namespace BookFinder\Helpers;

use BookFinder\Entities\Price;
use BookFinder\Interfaces\Enums\CurrencyEnum;

class Currency
{
    public static function convertPrice(Price $priceSource, CurrencyEnum $currencyDestination): Price
    {
        if ($priceSource->getCurrency() === $currencyDestination) {
            return $priceSource;
        }
        //TODO here would be the logic of conversion between currencies
        $convertedPrice = $priceSource;
        return $convertedPrice;
    }
}