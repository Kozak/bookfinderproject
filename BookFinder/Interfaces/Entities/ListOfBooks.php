<?php

declare(strict_types=1);

namespace BookFinder\Interfaces\Entities;

use BookFinder\Interfaces\Enums\CurrencyEnum;
use BookFinder\Helpers\Currency;

abstract class ListOfBooks implements \Iterator
{
    protected int $position;
    protected array $books = [];

    /**
     * @param array $books
     */
    public function __construct(array $books = [])
    {
        $this->position = 0;
        foreach ($books as $book) {
            $this->addBook($book);
        }
    }

    public function addBooks(ListOfBooks $books): void
    {
        foreach ($books as $book) {
            $this->addBook($book);
        }
    }

    public function addBook(Book $book): void
    {
        $this->books[] = $book;
    }

    abstract public function sort(CurrencyEnum $sortCurrency = CurrencyEnum::EUR): void;

    public function current(): Book
    {
        return $this->books[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->books[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }
}
