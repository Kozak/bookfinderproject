<?php

declare(strict_types=1);

namespace BookFinder\Interfaces\Entities;

use BookFinder\Exceptions\MissingConfigKey;

abstract class Config
{
    protected array $configData;

    public function addKeyValue(string $key, string $value): self
    {
        $this->configData[$key] = $value;
        return $this;
    }

    public function getValue(string $key): string
    {
        if (!array_key_exists($key, $this->configData)) {
            throw new MissingConfigKey($key);
        }
        return $this->configData[$key];
    }
}