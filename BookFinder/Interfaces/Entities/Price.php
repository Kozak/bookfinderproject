<?php

declare(strict_types=1);

namespace BookFinder\Interfaces\Entities;

use BookFinder\Interfaces\Enums\CurrencyEnum;

abstract class Price
{
    protected float $amount;
    protected CurrencyEnum $currency;

    /**
     * @param float $amount
     * @param string $currency
     */
    public function __construct(float $amount, string $currency)
    {
        $this->amount = $amount;
        $currency = CurrencyEnum::symbolToEnum($currency);
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): CurrencyEnum
    {
        return $this->currency;
    }
}