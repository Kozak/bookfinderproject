<?php

declare(strict_types=1);

namespace BookFinder\Interfaces\Entities;

abstract class ListOfBookstores implements \Iterator
{

    protected int $position;
    protected array $bookstores = [];

    /**
     * @param array $bookstores
     */
    public function __construct(array $bookstores = [])
    {
        $this->position = 0;
        foreach ($bookstores as $bookstore) {
            $this->addBookstore($bookstore);
        }
    }

    public function addBookstore(Bookstore $bookstore): void
    {
        $this->bookstores[] = $bookstore;
    }

    public function current(): Bookstore
    {
        return $this->bookstores[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): mixed
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->bookstores[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }
}