<?php

declare(strict_types=1);

namespace BookFinder\Interfaces\Entities;

abstract class Bookstore
{
    protected string $name;
    protected string $implemName;

    /**
     * @param string $name
     * @param string $key
     */
    public function __construct(string $name, string $implemName)
    {
        $this->name = $name;
        $this->implemName = $implemName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getImplemName(): string
    {
        return $this->implemName;
    }
}