<?php

declare(strict_types=1);

namespace BookFinder\Interfaces\Entities;

abstract class Book
{
    protected string $name;
    protected Price $price;
    protected Bookstore $store;

    /**
     * @param string $name
     * @param Price $price
     * @param Bookstore $store
     */
    public function __construct(string $name, Price $price, Bookstore $store)
    {
        $this->name = $name;
        $this->price = $price;
        $this->store = $store;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Price
     */
    public function getPrice(): Price
    {
        return $this->price;
    }

    /**
     * @return Bookstore
     */
    public function getStore(): Bookstore
    {
        return $this->store;
    }
}