<?php

declare(strict_types=1);

namespace BookFinder\Interfaces;

use BookFinder\Interfaces\Entities\Config;
use BookFinder\Interfaces\Entities\ListOfBooks;
use BookFinder\Interfaces\Entities\ListOfBookstores;
use BookFinder\Interfaces\Enums\CurrencyEnum;

interface BookFinder
{
    public function __construct(Config $config);

    public function searchBook(
        string $name,
        CurrencyEnum $sortCurrency = CurrencyEnum::EUR,
        ?ListOfBookstores $onlyBookStoresImplems = null
    ): ListOfBooks;
}