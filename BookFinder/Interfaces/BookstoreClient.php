<?php

declare(strict_types=1);

namespace BookFinder\Interfaces;

use BookFinder\Interfaces\Entities\Bookstore;
use BookFinder\Interfaces\Entities\Config;
use BookFinder\Interfaces\Entities\ListOfBooks;

interface BookstoreClient
{
    public function __construct(Config $config);

    public static function getBookStore(): Bookstore;

    public function listOfBooks(string $searchName): ListOfBooks;
}
