<?php

declare(strict_types=1);

namespace BookFinder\Interfaces\Enums;

enum CurrencyEnum: string
{
    case EUR = 'EUR';
    case USD = 'USD';
    case CZK = 'CZK';

    public static function symbolToEnum($symbol)
    {
        if (in_array($symbol,  self::cases())) {
            return $symbol;
        }
        return match ($symbol) {
            '€' => self::EUR,
            '$' => self::USD,
            'Kč' => self::CZK
        };
    }
}
