<?php

declare(strict_types=1);

namespace BookFinder\Entities;

use BookFinder\Exceptions\MissingConfigKey;
use BookFinder\Helpers\Currency;

class Config extends \BookFinder\Interfaces\Entities\Config
{
}