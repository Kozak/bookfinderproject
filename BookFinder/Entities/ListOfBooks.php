<?php

declare(strict_types=1);

namespace BookFinder\Entities;

use BookFinder\Interfaces\Entities\Book;
use BookFinder\Interfaces\Enums\CurrencyEnum;
use BookFinder\Helpers\Currency;

class ListOfBooks extends \BookFinder\Interfaces\Entities\ListOfBooks
{
    public function sort(CurrencyEnum $sortCurrency = CurrencyEnum::EUR): void
    {
        usort(
            $this->books,
            function (Book $a, Book $b) use ($sortCurrency) {
                if ($a->getName() !== $b->getName()) {
                    return strcmp($a->getName(), $b->getName());
                }
                /* Different bookstores may have a price in a different currency. That's why we have to convert them into a single currency for sorting */
                $priceA = Currency::convertPrice($a->getPrice(), $sortCurrency);
                $priceB = Currency::convertPrice($b->getPrice(), $sortCurrency);
                return $priceA->getAmount() > $priceB->getAmount();
            }
        );
    }
}
